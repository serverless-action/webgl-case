/**
 * @author alteredq / http://alteredqualia.com/
 * @authod mrdoob / http://mrdoob.com/
 * @authod arodic / http://aleksandarrodic.com/
 * @authod fonserbc / http://fonserbc.github.io/
*/

THREE.StereoEffect = function ( renderer ) {//分成两个窗口渲染物体 多视口渲染

	var _stereo = new THREE.StereoCamera();
	_stereo.aspect = 0.5;

	this.setEyeSeparation = function ( eyeSep ) {

		_stereo.eyeSep = eyeSep;

	};

	this.setSize = function ( width, height ) {

		renderer.setSize( width, height );

	};

	this.render = function ( scene, camera ) {

		scene.updateMatrixWorld();

		if ( camera.parent === null ) camera.updateMatrixWorld();

		_stereo.update( camera );

		var size = renderer.getSize();

		if ( renderer.autoClear ) renderer.clear();
		renderer.setScissorTest( true );//开启视口裁剪

		renderer.setScissor( 0, 0, size.width / 2, size.height );//设置裁剪区域 第一个参数：左下角x坐标，第二个参数：左下角y坐标，第三个参数：裁剪的宽，第四个参数：裁剪的高
		renderer.setViewport( 0, 0, size.width / 2, size.height );//设置视口变换 第一个参数：左下角x坐标，第二个参数：左下角y坐标，第三个参数：视口的宽，第四个参数：视口的高
		renderer.render( scene, _stereo.cameraL );

		renderer.setScissor( size.width / 2, 0, size.width / 2, size.height );
		renderer.setViewport( size.width / 2, 0, size.width / 2, size.height );
		renderer.render( scene, _stereo.cameraR );

		renderer.setScissorTest( false );//禁用视口裁剪

	};

};
